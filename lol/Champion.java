public class Champion {
    /** Nom du champion */
    private String nom;
    /** Cet attribut modélise les points de vie du champion */
    private int pointsDeVie;
    /** Cet attribut modélise les points d'attaque du champion */
    private int attaque;
    /** Cet attribut modélise la capacité de soin d'un champion */
    private int soin;
    /** permet de créer le champion selon des caractéristiques*/
    public Champion(String nom, int pointsDeVie, int attaque, int soin) {
        this.nom = nom;
        this.pointsDeVie = pointsDeVie;
        this.attaque = attaque;
        this.soin = soin;
    }
    /** Cette méthode permet de faire combattre deux champions */
    public void combat(Champion adversaire) {
        this.pointsDeVie -= adversaire.attaque;
        adversaire.pointsDeVie -= this.attaque;
    }
    /** Cette méthode permet de faire boire une potion à un champion */
    public void boitUnePotionDeSoin() {
        this.pointsDeVie += 5;
    }
    /** Cette méthode permet de soigner un champion ayant subi des dégâts */
    public void soigne(Champion championBlesse){
        championBlesse.pointsDeVie += this.soin;  
    }
    /** Cette méthode vérifie si le champion est encore en vie */
    public boolean estEnVie(){
        return this.pointsDeVie > 0;
    }
    /** Cette méthode affiche le champion qui a gagné avec ses caractéristiques */
    public String toString(){
        return "(" + this.nom + " : " + "pv = " + this.pointsDeVie + ", " + "attaque = " + this.attaque + ", " + "soin = " + this.soin; 
    }
}
