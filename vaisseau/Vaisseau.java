public class Vaisseau {
    /** Nom du vaisseau */
    private String nom;
    /* * Cet attribut modélise le nombre de passagers du vaisseau */
    private int nombreDePassagers = 0;
    /**  Cet attribut modélise la puissance du vaisseau*/
    private int puissanceDeFeu;
    /**  permet de construire un vaissaeau plus ou moins puissant*/
    public Vaisseau(String nom, int puissance) {
        this.nom = nom;
        this.puissanceDeFeu = puissance;
    }
    /** permet de créer un vaisseau plus ou moins puissant avec un nombre de passagers prédifini*/
    public Vaisseau(String nom, int puissance, int passagers) {
        this.nom = nom;
        this.puissanceDeFeu = puissance;
        this.nombreDePassagers = passagers;
    }
    /** permet d'obtenir le nom du vaisseau */
    public String getNom(){
        return this.nom;
    }
    /** permet d'obtenir le nombre de passagers */
    public int getNombrePassagers() {
        return this.nombreDePassagers;
    }
    /** permet d'obtenir la puissance du vaisseau*/
    public int getPuissance() {
        return this.puissanceDeFeu;
    }
    /** permet d'obtenir le nombre de passagers du vaisseau */
    public int nombreDePassagers() {
        return this.nombreDePassagers;
    }
    /** permet de vérifier si le nombre de passager maximal n'est pas dépassé */
    public boolean transportePassagers() {
        return nombreDePassagers > 0;
    }
}
